Supporting experiments for Lise Getoor's 2017 NIPS invited talk: "The Unreasonable Effectiveness of Structure".

Several of these experiments originated from a core JMLR paper by Stephen Bach.
The code can be found here: [github.com/stephenbach/bach-jmlr17-code](https://github.com/stephenbach/bach-jmlr17-code).
The paper can be cited as follows:
```
@article{bach:jmlr17,
  Author = {Bach, Stephen H. and Broecheler, Matthias and Huang, Bert and Getoor, Lise},
  Journal = {Journal of Machine Learning Research (JMLR)},
  Title = {Hinge-Loss {M}arkov Random Fields and Probabilistic Soft Logic},
  Year = {2017}
}
```

If you are looking for simplified versions of these experiments, then checkout the `PSL examples` repository at: [github.com/linqs/psl-examples](https://github.com/linqs/psl-examples).

## Running the Code

The `runAll.sh` script in the project's root will run each experiment in turn.
Inside each experiment directory, there is a a `run.sh` script that will run just that experiment.

All scripts have been tested on Linux, but should also work fine on Mac.
The run scripts do not explicitly support Windows, but all core components (ie PSL) supports Windows.
Therefore, one can manually follow the steps in the run scripts and translate them to Windows equivalents.


## Requirements

The base requirements for these experiments are:

* `curl` or `wget` - for fetching data and jars
* `time` - for timing experiments
* `tar` - for extracting data
* `java` - to run PSL
* `ruby` - for various support scripts

PostgreSQL is optionally required, along with a database called `psl` that the running user can connect to with local trust (no password when connecting on the same machine).
By default, all experiments with PSL will perform a run using the H2 embedded database as well the PostgreSQL database named `psl`.
If you do not have PostgreSQL installed, then runs requiring PostgreSQL will just quietly fail and the next run will start.

If you wish to run Tuffy experiments, then PostgreSQL is a hard requirement.
Tuffy requires the connecting user to have admin rights.
`scripts/tuffy.conf` contains the default connection information for Tuffy.

Any additional dependencies (eg PSL jars or the experimental data) will be fetched when the experiment is run.

Each experiment may include an additional readme file with any additional instructions or notes.


## Useful Scripts

Each experiment directory contains several useful scripts:

* `<experiment dir>/run.sh`- Fetches all code/data dependencies and start the runs for this experiment.
* `<experiment dir>/scripts/computeResults.rb`- Get accuracy information from the runs of this experiment.
* `<experiment dir>/scripts/fetchData.sh`- Fetches the data for this experiment. This is automatically called by `run.sh`, but users may find it useful if they want to fetch the data themselves.
* `<experiment dir>/scripts/parseTimingResults.rb`- Gets the basic timing information of the runs in this experiment.

There are also some scripts at the top level that you may find useful:

* `<root dir>/scripts/getExperimentResults.rb`- Attempts to parse and collect results for the accuracy-based experiments shown in this talk. Incomplete runs may cause this script to crash, so it is recommended that you remove the directories for incomplete runs before running this.
* `<root dir>/scripts/parseTimingResults.rb`- Looks for directories that look like output from an experimental run and will attempt to parse the standard timing information out of it and print it to the screen. It will also aggregate folds and print those out.
