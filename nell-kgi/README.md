This experiment is based off of Pujara et al.'s work on knowledge graph construction.
The code for the original experiments can be found here: https://github.com/linqs/KnowledgeGraphIdentification

The paper can be cited as follows:
```
@conference {pujara:iswc13,
  title = {Knowledge Graph Identification},
  booktitle = {International Semantic Web Conference (ISWC)},
  year = {2013},
  note = {Winner of Best Student Paper award},
  author = {Pujara, Jay and Miao, Hui and Getoor, Lise and Cohen, William}
}
```
